

							rsset 0
obj_x					rs.l 1
obj_y					rs.l 1
obj_vx				rs.l 1
obj_vy				rs.l 1
obj_func			rs.l 1
obj_health 		rs.w 1	;	if health is zero we can reuse this
obj_sz				rs.w 0

num_objs			=	100				; number of objects
gravity				= $00000010	; acceleration due to gravity

;;
;;	draw_objects
;;	process the object list
;;	run the "logic"
;;	if visible then add to the sprite list
;;	
draw_objects:

;	point to temporary memory to build sprite list

	lea.l	SPRITETEMP,a1											
;	a0 points to the objects	
	lea		objects,a0												
;	d7 is the offset of current object
	move.l	#((obj_sz*num_objs)-obj_sz),d7
;	d6 is sprite index	
	move.w 	#1,d6														; index
draw_loop:
	; get the object health, if it's zero then don't add to sprite list 
	;	or run logic

	move.w	obj_health(a0,d7.l),d0
	cmp #$00,d0
	beq			todrawnext

;	check y velocity for max speed
	move.l 		obj_vy(a0,d7.l),d0
	ADD.l			#gravity,d0
	cmp.l			#$400,d0
	bmi				dontcap
	move.l		#$400,d0
dontcap

;	simple y bounce logic
	move.l		d0,obj_vy(a0,d7.l)
;	add x vel to x pos
	move.l	obj_x(a0,d7.l),d0
	add.l	obj_vx(a0,d7.l),d0
	move.l	d0,obj_x(a0,d7.l)
;	add y vel to y pos
	move.l	obj_y(a0,d7.l),d0
	add.l	obj_vy(a0,d7.l),d0
	move.l	d0,obj_y(a0,d7.l)

;	check for our fake floor 
	move.l	obj_y(a0,d7.l),d0
	cmp.l #(512)<<8,d0
	bmi nobounce
;	set the object to the floor	
	move.l #(512)<<8,obj_y(a0,d7.l)
;	flip the Y velocity 	
	move.l	obj_vy(a0,d7.l),d0
	neg.l		d0
	move.l	d0,obj_vy(a0,d7.l)	
nobounce

	;	check we have enough indices left to even consider rendering
	cmp			#80,d6
	beq			todrawnext

	;	here we check it's onscreen 
	;	before wasting a sprite
	move.l	obj_x(a0,d7.l),d0
	;	remove lower 8 bits , the FP24:8 
	ror.l 	#8,d0
	;	adjust for playscreen 1	
	add			PLAY1X,d0
	and 		#$7ff,d0
	;	check left edge
	cmp.w		#128-8,d0
	bmi			todrawnext
	;	check right edge
	cmp.w		#128+320+8,d0
	bpl			todrawnext
	;	save this position to D5
	move.l		d0,d5	;	save X

;	copy object Y
	move.l	obj_y(a0,d7.l),d0	;	y first
;	remove lower 8 bits , the FP24:8	
	ror.l			#8,d0
;	adjust for playscreen 1	
	sub.l			PLAY1Y,d0
;	store in sprite table	
	move.w	d0,(a1)+	

;	for the link 
	move.w	d6,d0			;	next index
;	what size sprite we want TODO add this to the struct
	add 		#S_1X1,d0	;	flags
;	store in sprite table	
	move.w	d0,(a1)+	;	store it 

;	animation frame
	move.w FRAME,d0	
;	adjust for palette, and base cell	
	add #S_PAL2+0x301,d0	
;	adjust for FLIP	
	add.w	SONICD,d0
;	store in sprite table	
	move.w	d0,(a1)+			
;	now copy the saved X to the sprite table
	move.w	d5,(a1)+			
;	sprite index	
	add.w		#1,d6					
todrawnext:
; decrement to previous object
	sub.l		#obj_sz,d7		
; loop if not all done	
	bpl			draw_loop			
exitsprites:
	move.l	#$10000,(a1)+	;terminate sprite list
	move.l	#1,(a1)+			;	"  "
	rts


; find next free object
;	will always return sprite zero if it fails, D7 will be the offset
next_free_object:
; a0 points to the objects
	lea		objects,a0						
;	set the index to the last object
	move.l	#((obj_sz*num_objs)-obj_sz),d7

find_loop:
	; get the object health
	move.w	obj_health(a0,d7.w),d0
	bne			tonext
	;	if heath is zero then leave the loop
	jmp 		leave
tonext:
; decrement to previous object
	sub.w		#obj_sz,d7
; loop if not all done	
	bpl			find_loop
	move.l 	#0,d7
leave:	
	rts

reset_objects:
	; a0 points to the objects
	lea		objects,a0
;	set the index to the last object
	move.l	#((obj_sz*num_objs)-obj_sz),d7
;	give us a fake X position	
	move.l	#($80)<<8,d6
;	give us a fake Y position	
	move.l	#0,d5
	
reset_loop:
;	set up object
	move.w 	#1,obj_health(a0,d7.l)
	move.l	d6,obj_x(a0,d7.l)
	move.l	d5,obj_y(a0,d7.l)
;	adjust the fake X and Y 
	add.l		#($12)<<8,d6
	add.l		#($8)<<8,d5
	and.l		#$ffff,d5
; decrement to previous object
	sub.w		#obj_sz,d7		
; loop if not all done	
	bpl			reset_loop			
	rts
