// Dictionary codec interface for vgmpack
// Mic, 2009

#ifndef _DICTCODEC_H_
#define _DICTCODEC_H_

#include "codec.h"
#include <vector>

using namespace std;

#define DICT_SLOT_EMPTY 0x100

#define DICT_SIZE 0x100		// It's best to leave this at 0x100 since only one-byte lengths are written

#define DICT_THRESHOLD 48


class DictCodec : public Codec
{
public:
	~DictCodec() { if (altCodec) delete altCodec; }

	DictCodec() { run = 0; }
	
	// Write the output data to a char vector
	DictCodec(vector<unsigned char> *data)
	{
		outData = data;
		altCodec = NULL;
		run = 0;
		for (int i = 0; i < DICT_SIZE; i++)
		{
			dictionary[i] = DICT_SLOT_EMPTY;
		}
		lastStoredInDict = DICT_SLOT_EMPTY;
		numCharsInDict = 0;
	}

	
	void passThrough(unsigned char c);	// Write data to output stream unencoded
	void write(unsigned char c);		// Encode data and write to output stream
	void flush();						// Flush any encoded data to the output stream
	void *get(int g)
	{
		if (g == 0)
		{
			return dictionary;
		}
		return NULL;
	}

private:
	void writeHelper(unsigned char c);

	int run;
	unsigned int numCharsInDict;
	unsigned short lastStoredInDict;
	unsigned short dictionary[DICT_SIZE];
	vector<unsigned char> stringToMatch;
	vector<unsigned char> candidates;
	vector<unsigned char> *outData;
};


#endif
