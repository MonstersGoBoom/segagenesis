..\..\bin\sixpack.exe -pack -image -opt -target smd -width 256 -height 224 -sizefilter 1 -o maruko.lzs -preview preview.bmp ..\images\maruko.png
m68k-elf-as -m68000 --register-prefix-optional --bitwise-or -o sega_gcc.o sega_gcc.s
m68k-elf-as -m68000 --register-prefix-optional --bitwise-or -o test.o test_lzss.s
m68k-elf-as -m68000 --register-prefix-optional --bitwise-or -o lzss_decode.o ..\..\decoder\smd\lzss_decode.s
m68k-elf-ld -Tmd.ld --oformat binary -o test_lzss.bin sega_gcc.o test.o lzss_decode.o

..\..\bin\sixpack.exe -pack -codec aplib -image -opt -target smd -width 256 -height 224 -sizefilter 1 -o maruko.apx -preview preview.bmp ..\images\maruko.png
m68k-elf-as -m68000 --register-prefix-optional --bitwise-or -o sega_gcc.o sega_gcc.s
m68k-elf-as -m68000 --register-prefix-optional --bitwise-or -o test.o test_aplib.s
m68k-elf-as -m68000 --register-prefix-optional --bitwise-or -o aplib_decrunch.o ..\..\decoder\smd\aplib_decrunch.s
m68k-elf-ld -Tmd.ld --oformat binary -o test_aplib.bin sega_gcc.o test.o aplib_decrunch.o

del *.o
del maruko.apx
del maruko.lzs
del maruko.pal
del maruko.nam
