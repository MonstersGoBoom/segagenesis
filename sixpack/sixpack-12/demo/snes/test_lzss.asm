; Test program for pattern loading
; SNES version
; /Mic, 2009

.include "header.inc"
.include "Snes_Init.asm"
.include "snes.inc"
.include "snes_ppu.inc"


VBlank:    ; Needed to satisfy interrupt definition in "header.inc"
    rti


.bank 0
.section "MainCode"

Start:
	; Initialize the SNES.
	Snes_Init

	sep     #(A_8BIT|XY_8BIT)

	lda     #BLANK_SCREEN  	; Force VBlank by turning off the screen.
	sta     REG_DISPCNT

    
  	sep 	#A_8BIT
  	lda 	#:patterns
  	phb 			; Save old DBR 
  	pha
  	plb
  	rep 	#(A_8BIT|XY_8BIT)
  	lda 	#$4000		; Decode to VRAM address 0000
  	ldx 	#patterns
  	ldy 	#(patterns_end-patterns)
  	jsr 	lzss_decode_vram
  	plb			; Restore old DBR

    	SetPalette palette,0,512
    	LoadVRAM map,$0000,(map_end-map)
    
    	sep     #A_8BIT

	; Set display mode 3 
	lda	#3
	sta     REG_DISPMODE

	; Use BG0, which is 8bpp in mode 3
	lda     #BG0_ENABLE
	sta     REG_BGCNT

	; Set the pattern base address for BG0 and BG1 to $8000
	lda	#$44
	sta	REG_CHRBASE_L

	; Set the map base address for BG0 to $0000 and the map size to 32x32 tiles
	stz     REG_BG0MAP

	lda     #$0F  		; End VBlank, setting brightness to 15 (100%).
	sta     REG_DISPCNT

    	; Loop forever.
Forever:
	jmp 	Forever


; Set some flags used by the decoder
.DEFINE LZSS_DICTIONARY_SIZE 4096
.DEFINE LZSS_THRESHOLD 2
.DEFINE LZSS_LEN_BITS 4
.DEFINE LZSS_LEN_MASK $0f
.DEFINE LZSS_MAX_LEN 18
.DEFINE LZSS_PLANES_USED 4
.DEFINE LZSS_FORMAT_PLANES 4

.include "..\..\decoder\snes\lzss_decode.asm"

patterns:
 .incbin "parodius2.lzs"
patterns_end:

palette:
 .incbin "parodius2.pal"
palette_end:

map:
 .incbin "parodius2.nam"
map_end:

.ends
