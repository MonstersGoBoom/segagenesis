.equ REG_DISPCNT $2100
.equ REG_DISPMODE $2105

.equ REG_BG0MAP $2107
.equ REG_BG1MAP $2108
.equ REG_BG2MAP $2109
.equ REG_BG3MAP $210A

.equ REG_CHRBASE_L $210B
.equ REG_CHRBASE_H $210C

.equ REG_VRAM_INC $2115
.equ REG_VRAM_ADDR_L $2116
.equ REG_VRAM_ADDR_H $2117
.equ REG_VRAM_DATAW1 $2118
.equ REG_VRAM_DATAW2 $2119
.equ REG_VRAM_DATAR1 $2139
.equ REG_VRAM_DATAR2 $213A

.equ REG_CGRAM_ADDR $2121
.equ REG_CGRAM_DATAW $2122

.equ REG_BGCNT $212C

.equ BLANK_SCREEN $80

.equ BG0_ENABLE $01
.equ BG1_ENABLE $02
.equ BG2_ENABLE $04
.equ BG3_ENABLE $08
.equ OBJ_ENABLE $10


;macro for loading palette data into the CGRAM
;syntax SetPalette LABEL CGRAM_ADDRESS SIZE
.macro SetPalette
pha
php

rep	#$20		; 16bit A
lda	#\3
sta	$4305		; # of bytes to be copied
lda	#\1		; offset of data into 4302, 4303
sta	$4302
sep	#$20		; 8bit A

lda	#:\1		; bank address of data in memory(ROM)
sta	$4304	
lda	#\2
sta	REG_CGRAM_ADDR	; address of CGRAM to start copying graphics to

stz	$4300		; 0= 1 byte increment (not a word!)
lda	#<REG_CGRAM_DATAW
sta	$4301		; destination 21xx   this is 2122 (CGRAM Gate)

lda	#$01		; turn on bit 1 (corresponds to channel 0) of DMA channel reg.
sta	$420b		;   to enable transfer

plp
pla
.endm



;macro for loading graphics data into the VRAM
;syntax LoadVRAM LABEL  VRAM_ADDRESS  SIZE
.macro LoadVRAM
pha
php

sep	#$20
lda	#<(\2 / 2)
sta	REG_VRAM_ADDR_L	; address for VRAM write(or read)
lda	#>(\2 / 2)
sta	REG_VRAM_ADDR_H	; address for VRAM write(or read)
lda	#<\3
sta	$4305		; number of bytes to be copied
lda	#>\3
sta	$4306		; number of bytes to be copied
rep	#$20
lda	#\1		
sta	$4302		; data offset in memory
sep	#$20		; 8bit A

lda	#:\1		; bank address of data in memory
sta	$4304	
lda	#$80  
sta	REG_VRAM_INC	; VRAM address increment value designation
lda	#$01
sta	$4300		; 1= word increment
lda	#<REG_VRAM_DATAW1
sta	$4301		; 2118 is the VRAM gate

lda	#$01		; turn on bit 1 (channel 0) of DMA
sta	$420b
 
plp
pla
.endm



