; Test program for pattern loading
; SNES version
; /Mic, 2009

.include "header.inc"
.include "Snes_Init.asm"
.include "snes.inc"
.include "snes_ppu.inc"


VBlank:    ; Needed to satisfy interrupt definition in "header.inc"
    rti


.bank 0
.section "MainCode"

Start:
	; Initialize the SNES.
	Snes_Init

	sep     #(A_8BIT|XY_8BIT)

	lda     #BLANK_SCREEN  	; Force VBlank by turning off the screen.
	sta     REG_DISPCNT

	; Decrunch pattern data to RAM at $7f6000
	sep 	#A_8BIT
	lda 	#$7f
	phb 					; Save old DBR 
	pha
	plb
	lda		#$40
	rep 	#XY_8BIT
	ldx 	#$6000			; Decode to $7f6000
	ldy 	#patterns
	jsr 	aplib_decrunch
	plb						; Restore old DBR

	; Unchain the decompressed tile data and copy it to VRAM
	sep 	#A_8BIT
	lda 	#$7f
	ldx 	#$4000			; Copy to VRAM address $4000 (byte address $8000)
	ldy 	#$6000			; Copy from RAM address $7f6000
	jsr		aplib_unchain_p8_copy_to_vram
	
	
	SetPalette palette,0,512
	LoadVRAM map,$0000,(map_end-map)

	sep     #A_8BIT

	; Set display mode 3 
	lda	#3
	sta     REG_DISPMODE

	; Use BG0, which is 8bpp in mode 3
	lda     #BG0_ENABLE
	sta     REG_BGCNT

	; Set the pattern base address for BG0 and BG1 to $8000
	lda	#$44
	sta	REG_CHRBASE_L

	; Set the map base address for BG0 to $0000 and the map size to 32x32 tiles
	stz     REG_BG0MAP

	lda     #$0F  		; End VBlank, setting brightness to 15 (100%).
	sta     REG_DISPCNT

    	; Loop forever.
Forever:
	jmp 	Forever


.include "..\..\decoder\snes\aplib_decrunch.asm"

patterns:
 .incbin "parodius2.apl"
patterns_end:

palette:
 .incbin "parodius2.pal"
palette_end:

map:
 .incbin "parodius2.nam"
map_end:

.ends
