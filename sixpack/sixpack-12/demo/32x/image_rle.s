.text
.globl _maruko
.globl _maruko_end
.globl _palette
.globl _palette_end
.global _maruko_ltb

_maruko:
.incbin "maruko.rle"
_maruko_end:

_palette:
.incbin "maruko.pal"
_palette_end:

_maruko_ltb:
.incbin "maruko.ltb"