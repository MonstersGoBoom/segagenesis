; Created by Sixpack on 04/18/10 15:47:30

.DEFINE LZSS_DICTIONARY_SIZE 4096
.DEFINE LZSS_THRESHOLD 2
.DEFINE LZSS_LEN_BITS 4
.DEFINE LZSS_LEN_MASK $0f
.DEFINE LZSS_MAX_LEN 18
.DEFINE LZSS_PLANES_USED 3
.DEFINE LZSS_FORMAT_PLANES 4

ufolog_pattern:
.incbin "ufolog.lzs"
ufolog_pattern_end:

ufolog_pattern_decode:
ld ix,ufolog_pattern
ld iy,ufolog_pattern_end-ufolog_pattern
call lzss_decode_vram
ret
