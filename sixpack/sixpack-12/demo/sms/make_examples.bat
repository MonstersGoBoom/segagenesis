..\..\bin\sixpack.exe -pack -image -opt -target sms -t -width 128 -height 128 -sizefilter 1 -planes 0 -code -o ufolog.lzs -preview preview.bmp ..\images\ufolog.png
wla-z80 -vo -DLZSS_SUPPORT_BITPLANE_OPTIMISATION test_lzss.asm test.o
wlalink -b test.link test_lzss.sms

..\..\bin\sixpack.exe -pack -codec aplib -image -opt -v -target sms -t -width 128 -height 128 -sizefilter 1 -planes 4 -code -o ufolog.apx -preview preview.bmp ..\images\ufolog.png
wla-z80 -vo test_aplib.asm test.o
wlalink -b test.link test_aplib.sms

del test.o
del ufolog.apx
del ufolog.lzs
del ufolog.nam
del ufolog.pal