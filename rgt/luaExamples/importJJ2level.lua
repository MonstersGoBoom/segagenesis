function strti(i)
	return i:byte(1,1)|(i:byte(2,2)<<8)|(i:byte(3,3)<<16)|(i:byte(4,4)<<24)
end
function strts(i)
	return i:byte(1,1)|(i:byte(2,2)<<8)
end
if project.have(project.levelMask|project.chunksMask|project.mapMask|project.tilesMask) then
	local fname=fl.file_chooser("Load j2l",'*.j2l')
	if not (fname == nil or fname == '') then
		local file=assert(io.open(fname,"rb"))
		local str=file:read('*a')
		--[[char Copyright[180]; (1,180)
		char Magic[4] = "LEVL"; (181,184)
		char PasswordHash[3]; // 0xBEBA00 for no password (185,187)
		char HideLevel; (188,188)
		char LevelName[32]; (189,220)
		short Version; (221,222)
		long FileSize; (223,226)
		long CRC32; (227,230)
		long CData1;            // compressed size of Data1 (231,234)
		long UData1;            // uncompressed size of Data1 (235,238)
		long CData2;            // compressed size of Data2 (239,242)
		long UData2;            // uncompressed size of Data2 (243,246)
		long CData3;            // compressed size of Data3 (247,250)
		long UData3;            // uncompressed size of Data3 (251,254)
		long CData4;            // compressed size of Data4 (255,258)
		long UData4;            // uncompressed size of Data4 (259,262)--]]
		io.close(file)
		if str:sub(181,184)~='LEVL' then
			fl.alert('The magic signature is wrong')
		end
		print('Level name',str:sub(189,220))
		Cdat1=strti(str:sub(231,234))
		Cdat2=strti(str:sub(239,242))
		Cdat3=strti(str:sub(247,250))
		Udat3=strti(str:sub(251,254))
		Cdat4=strti(str:sub(255,258))
		Cdat4=strti(str:sub(255,258))
		cd1=zlib.inflate()(str:sub(263,263+Cdat1))
		--[[  short JCSHorizontalOffset; // In pixels (1,2)
		short Security1; // 0xBA00 if passworded, 0x0000 otherwise (3,4)
		short JCSVerticalOffset; // In pixels (5,6)
		short Security2; // 0xBE00 if passworded, 0x0000 otherwise (7,8)
		char SecAndLayer; // Upper 4 bits are set if passworded, zero otherwise. Lower 4 bits represent the layer number as last saved in JCS. (9,9)
		char MinLight; // Multiply by 1.5625 to get value seen in JCS (10,10)
		char StartLight; // Multiply by 1.5625 to get value seen in JCS (11,11)
		short AnimCount; (12,13)
		bool VerticalSplitscreen; (14,14)
		bool IsLevelMultiplayer; (15,15)
		long BufferSize; (16,19)
		char LevelName[32]; (20,51)
		char Tileset[32]; (52,83)
		char BonusLevel[32]; (84,115)
		char NextLevel[32]; (116,147)
		char SecretLevel[32]; (148,179)
		char MusicFile[32]; (180,211)
		char HelpString[16][512]; (212,8403)
		char SoundEffectPointer[48][64]; // only in version 256 (AGA)
		long LayerMiscProperties[8]; // Each property is a bit in the following order: Tile Width, Tile Height, Limit Visible Region, Texture Mode, Parallax Stars. This leaves 27 (32-5) unused bits for each layer? (8404,8435)
		char "Type"[8]; // name from Michiel; function unknown (8436,8443)
		bool DoesLayerHaveAnyTiles[8]; // must always be set to true for layer 4, or JJ2 will crash (8444,8451)
		long LayerWidth[8]; (8452,8483)
		long LayerRealWidth[8]; // for when "Tile Width" is checked. The lowest common multiple of LayerWidth and 4. (8484,8515)
		long LayerHeight[8]; (8516,8547)
		long LayerZAxis[8] = {-300, -200, -100, 0, 100, 200, 300, 400}; // nothing happens when you change these
		char "DetailLevel"[8]; // is set to 02 for layer 5 in Battle1 and Battle3, but is 00 the rest of the time, at least for JJ2 levels. No clear effect of altering. Name from Michiel.
		int "WaveX"[8]; // name from Michiel; function unknown
		int "WaveY"[8]; // name from Michiel; function unknown
		long LayerXSpeed[8]; // Divide by 65536 to get value seen in JCS
		long LayerYSpeed[8]; // Divide by 65536 to get value seen in JCSvalue
		long LayerAutoXSpeed[8]; // Divide by 65536 to get value seen in JCS
		long LayerAutoYSpeed[8]; // Divide by 65536 to get value seen in JCS
		char LayerTextureMode[8];
		char LayerTextureParams[8][3]; // Red, Green, Blue
		short AnimOffset; // MAX_TILES minus AnimCount, also called StaticTiles
		long TilesetEvents[MAX_TILES]; // same format as in Data2, for tiles
		bool IsEachTileFlipped[MAX_TILES]; // set to 1 if a tile appears flipped anywhere in the level
		char TileTypes[MAX_TILES]; // translucent=1 or caption=4, basically. Doesn't work on animated tiles.
		char "XMask"[MAX_TILES]; // tested to equal all zeroes in almost 4000 different levels, and editing it has no appreciable effect.  // Name from Michiel, who claims it is totally unused.
		char UnknownAGA[32768]; // only in version 256 (AGA)
		Animated_Tile Anim[128]; // or [256] in TSF.
		// only the first [AnimCount] are needed; JCS will save all 128/256, but JJ2 will run your level either way.
		char Padding[512]; //all zeroes; only in levels saved with JCS--]]
		print('Level name',cd1:sub(20,51))
		local valid,width,height,idx,validcnt,w4,inf,rw,i={},{},{},8404,0,{},{},{}
		for i=1,8 do
			inf[i]=strti(cd1:sub(idx,idx+3))
			print('Info layer:',i,'Tile width',inf[i]&1,'Tile height',(inf[i]>>1)&1,'Limit visible region',(inf[i]>>2)&1,'Texture mode',(inf[i]>>3)&1,'Parallax stars',(inf[i]>>4)&1,'inf',inf[i])
			idx=idx+4
		end
		idx=8444
		for i=1,8 do
			valid[i]=cd1:byte(idx,idx)~=0
			idx=idx+1
			print('Valid layer:',i,valid[i])
			if valid[i] then
				validcnt=validcnt+1
			end
		end
		print('validcnt',validcnt)
		for i=1,8 do
			width[i]=strti(cd1:sub(idx,idx+3))
			idx=idx+4
			print('Width layer:',i,width[i])
		end
		for i=1,8 do
			rw[i]=strti(cd1:sub(idx,idx+3))
			w4[i]=(rw[i]+3)//4
			idx=idx+4
			print('Real width layer:',i,rw[i])
		end
		for i=1,8 do
			height[i]=strti(cd1:sub(idx,idx+3))
			idx=idx+4
			print('Height layer:',i,height[i])
		end
		level.setLayerAmt(validcnt)
		basename=fl.filename_name(fname)
		local cnt=0
		for i=1,8 do
			if valid[i] then
				level.setLayerName(cnt,string.format("%s layer %d",basename,i))
				cnt=cnt+1
			end
		end
		print('Passed 1')
		cnt=0
		for i=1,8 do
			print(i)
			if valid[i] then
				level.resizeLayer(cnt,w4[i],height[i])
				local info=level.getInfo(cnt)
				info.src=level.CHUNKS
				cnt=cnt+1
			end
		end
		chunks.resize(4,1)
		chunks.setAmt(Udat3/8)
		print('Passed 2')
		--TODO read data2 in order to work with parameters for sprite features

		-- Read chunks
		cd3=zlib.inflate()(str:sub(263+Cdat1+Cdat2,263+Cdat1+Cdat2+Cdat3))
		idx=1
		for i=1,Udat3/8 do
			for j=0,3 do
				local tile=strts(cd3:sub(idx,idx+1))
				if (tile&1024)~=0 then
					chunks.setFlags(i-1,j,0,1)
				else
					chunks.setFlags(i-1,j,0,0)
				end
				chunks.setBlocks(i-1,j,0,tile&1023)
				idx=idx+2
			end
		end
		print('Passed 3')
		cd4=zlib.inflate()(str:sub(263+Cdat1+Cdat2+Cdat3,263+Cdat1+Cdat2+Cdat3+Cdat4))
		idx=1
		cnt=0
		for i=1,8 do
			print(i)
			if valid[i] then
				for y=0,height[i]-1 do
					for x=0,w4[i]-1 do
						local info=level.getXY(cnt,x,y)
						info.id=strts(cd4:sub(idx,idx+1))
						idx=idx+2
					end
				end
				cnt=cnt+1
			end
		end
		project.update()--Sync the level GUI with these changes
	end
	local fname=fl.file_chooser("Load j2t",'*.j2t')
	if not (fname == nil or fname == '') then
		
	end
else
	project.haveMessage(project.levelMask|project.chunksMask|project.mapMask|project.tilesMask)
end
